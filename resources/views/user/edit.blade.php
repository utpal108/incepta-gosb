@extends('layout.app')

@section('page_title','| Edit Profile')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
@endsection

@section('contents')

    <section id="consultant-breadcrum">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>EDIT PROFILE</h2>
                    <strong><a href="#">HOME</a> // <a href="#">EDIT PROFILE</a></strong>
                </div>
            </div>
        </div>
    </section>

    <form action="{{ action('UserController@update_profile') }}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
        @csrf
        <section id="consultant"><!-- Chairmen Message Start -->
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fileinput fileinput-new product-image ic-thumb-item" data-provides="fileinput" >
                            <div class="fileinput-new thumbnail" style="max-width: 350px; max-height: 350px;">
                                <img src="@if(!is_null($user->profile_image)){{ '/storage/' .$user->profile_image }} @else{{ 'http://placehold.it/400x400' }} @endif" width="100%" alt="profile image">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 350px; max-height: 350px;"></div>
                            <div class="text-center">
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="profile_image">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        @include('flash::message')
                        <div class="cons-pera">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $user->name ?? '' }}" required>
                            </div>
                            <div class="form-group">
                                <label>Designation:</label>
                                <input type="text" class="form-control" name="designation" placeholder="Designation" value="{{ $user->designation ?? '' }}" required>
                            </div>
                            <div class="form-group">
                                <label>About:</label>
                                <textarea rows="5" class="form-control" name="about" required>{{ $user->profile['about'] ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Speciality:</label>
                                <input type="text" class="form-control" placeholder="Speciality" name="specialist" value="{{ $user->specialist ?? '' }}" required>
                            </div>
                            <div class="form-group">
                                <label>Education:</label>
                                <textarea rows="3" class="form-control" name="education" required>{{ $user->profile['education'] ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Experience:</label>
                                <textarea rows="3" class="form-control" name="experience" required>{{ $user->profile['experience'] ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Awards & Recognition:</label>
                                <textarea rows="3" class="form-control" name="awards" required>{{ $user->profile['awards'] ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Address:</label>
                                <textarea rows="5" class="form-control" name="address" required>{{ $user->profile['address'] ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Phone:</label>
                                <input type="text" class="form-control" name="phone_no" value="{{ $user->phone_no ?? '' }}" placeholder="Phone No" required>
                            </div>
                            <div class="form-group">
                                <label>Website:</label>
                                <input type="url" class="form-control" name="website" value="{{ $user->profile['website'] ?? '' }}" placeholder="Website" required>
                            </div>
                            <div class="form-group">
                                <label>Social Links:</label>
                                <input type="url" name="social_links[facebook]" value="{{ $user->profile['social_links']['facebook'] ?? '' }}" class="form-control mt-3" placeholder="Facebook">
                                <input type="url" name="social_links[instagram]" value="{{ $user->profile['social_links']['instagram'] ?? '' }}" class="form-control mt-3" placeholder="Instagram">
                                <input type="url" name="social_links[youtube]" value="{{ $user->profile['social_links']['youtube'] ?? '' }}" class="form-control mt-3" placeholder="Youtube">
                                <input type="url" name="social_links[twitter]" value="{{ $user->profile['social_links']['twitter'] ?? '' }}" class="form-control mt-3" placeholder="Twitter">
                            </div>
                            <div class="uh-button ptd">
                                <button class="btn btn-default" type="submit">Update Profile</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section><!-- Chirmen message End -->
    </form>
@endsection

@section('script')
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
