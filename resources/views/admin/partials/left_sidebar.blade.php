
<div class="sa-aside-left">

    <a href="javascript:void(0)"  onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')" class="sa-sidebar-shortcut-toggle">
        <img src="/ic_admin/img/avatars/sunny.png" alt="" class="online">
        <span>{{ Auth::user()->name }} <span class="fa fa-angle-down"></span></span>
    </a>
    <div class="sa-left-menu-outer">
        <ul class="metismenu sa-left-menu" id="menu1">
            {{--            For Dashboard           --}}
            <li class="{{ request()->is('admin') ? 'active' : '' }}">
                <a class="has-arrow"   href="{{ action('Admin\DashboardController@index') }}" title="Dashboard"><span class="fa fa-lg fa-fw fa-home"></span> <span class="menu-item-parent">Dashboard</span></a>
            </li>

            {{--            For Slider            --}}
            <li class="{{ request()->is('admin/slider*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Slider</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@index') }}" title="Slider groups"> Slider groups </a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@create') }}" title="Create slider group"> Create slider group</a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/sliders') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@index') }}" title="Sliders"> Sliders </a>
                    </li>
                    <li class="{{ request()->is('admin/sliders/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@create') }}" title="Create slider"> Create slider </a>
                    </li>
                </ul>

            </li>

            {{--            For Subscribers            --}}
            <li class="{{ request()->is('admin/subscribers*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Subscribers</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/subscribers') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@index') }}" title="All Subscribers"> Subscribers </a>
                    </li>
                    <li class="{{ request()->is('admin/subscribers/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@create') }}" title="Create subscriber"> Create Subscriber</a>
                    </li>
                </ul>
            </li>

            {{--            For Categories            --}}
            <li class="{{ request()->is('admin/categories*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Category</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/categories') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\CategoryController@index') }}" title="All Categories"> Categories </a>
                    </li>
                    <li class="{{ request()->is('admin/categories/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\CategoryController@create') }}" title="Create Category"> Create Category</a>
                    </li>
                </ul>
            </li>

            {{--            For Blog Posts  --}}

            <li class="{{ request()->is('admin/blog-posts*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Blog"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Blog</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/blog-posts') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\BlogPostController@index') }}" title="Blog Posts"> Blog Posts</a>
                    </li>
                    <li class="{{ request()->is('admin/blog-posts/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\BlogPostController@create') }}" title="Add Blog Post"> Add New Post</a>
                    </li>
                </ul>

            </li>

            {{--            For Gallery Categories            --}}
            <li class="{{ request()->is('admin/gallery-categories*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Gallery Category</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/gallery-categories') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\GalleryCategoryController@index') }}" title="All Categories"> Categories </a>
                    </li>
                    <li class="{{ request()->is('admin/gallery-categories/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\GalleryCategoryController@create') }}" title="Create Category"> Create Category</a>
                    </li>
                </ul>
            </li>

            {{--            For Gallery            --}}
            <li class="{{ request()->is('admin/galleries*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Galleries</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/galleries') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\GalleryController@index') }}" title="All Gallery Elements"> Gallery Elements </a>
                    </li>
                    <li class="{{ request()->is('admin/galleries/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\GalleryController@create') }}" title="Create Gallery Element"> Create Element</a>
                    </li>
                </ul>
            </li>

            {{--            For Pages           --}}

            <li class="{{ request()->is('admin/pages*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="Page"><span class="fa fa-lg fa-fw fa-file"></span> <span class="menu-item-parent"> Page</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/pages') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@index') }}" title="All pages"> All pages</a>
                    </li>
                    <li class="{{ request()->is('admin/pages/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@create') }}" title="Create new page"> Create new page</a>
                    </li>
                </ul>

            </li>

            {{--            For Menu Builder           --}}

            <li class="{{ request()->is('admin/menu*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Menu</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/menu') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\MenuController@index') }}" title="menu builder"> Menu Builder </a>
                    </li>
                </ul>
            </li>


            {{--            For Site Settings          --}}

            <li class="{{ request()->is('admin/site-settings*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Site Settings</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/site-settings') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SiteSettingController@index') }}" title="site settings"> Edit Settings </a>
                    </li>
                </ul>
            </li>

            {{--            For Conference          --}}

            <li class="{{ request()->is('admin/conference*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Conference</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/conference-types') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ConferenceTypeController@index') }}" title="conference type"> All Types </a>
                    </li>
                    <li class="{{ request()->is('admin/conference-types/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ConferenceTypeController@create') }}" title="create conference type"> Add New Type </a>
                    </li>
                    <li class="{{ request()->is('admin/conferences') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ConferenceController@index') }}" title="conferences"> All Conferences </a>
                    </li>
                    <li class="{{ request()->is('admin/conferences/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ConferenceController@create') }}" title="create conference"> Add Conference </a>
                    </li>
                </ul>
            </li>

            {{--            For Forum Post        --}}

            <li class="{{ request()->is('admin/forum*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Forum</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/forums') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ForumController@index') }}" title="forums"> Forum Posts </a>
                    </li>
                    <li class="{{ request()->is('admin/forums/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ForumController@create') }}" title="create forums"> Add Forum Post </a>
                    </li>
                </ul>
            </li>

            {{--            For Widget        --}}

            <li class="{{ request()->is('admin/widgets*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Widget</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/widgets') ? 'active' : '' }}">
                        <a href="{{ action('Admin\WidgetController@index') }}" title="widgets"> Widgets </a>
                    </li>
                </ul>
            </li>

            {{--            For Events        --}}

            <li class="{{ request()->is('admin/events*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Event</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/events') ? 'active' : '' }}">
                        <a href="{{ action('Admin\EventController@index') }}" title="events"> All Events </a>
                    </li>
                    <li class="{{ request()->is('admin/events/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\EventController@create') }}" title="create event"> Create Event </a>
                    </li>
                </ul>
            </li>

            {{--            For Memberships        --}}

            <li class="{{ request()->is('admin/memberships*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Membership</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/memberships') ? 'active' : '' }}">
                        <a href="{{ action('Admin\MembershipController@index') }}" title="events"> All Memberships </a>
                    </li>
                    <li class="{{ request()->is('admin/memberships/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\MembershipController@create') }}" title="create event"> Create Membership </a>
                    </li>
                </ul>
            </li>

            {{--            For Pending Users        --}}

            <li class="{{ request()->is('admin/pending-users*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Pending Users</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/pending-users') ? 'active' : '' }}">
                        <a href="{{ action('Admin\PendingUserController@index') }}" title="pending users"> Pending Users </a>
                    </li>
                </ul>
            </li>

            {{--            For Doctors       --}}

            <li class="{{ request()->is('admin/doctor*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Doctors</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/doctor-consultants') ? 'active' : '' }}">
                        <a href="{{ action('Admin\DoctorController@index') }}" title="doctors"> Doctors </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
        <i class="fa fa-arrow-circle-left hit"></i>
    </a>
</div>
