@extends('admin.layout.app')

@section('page_title','Admin | Edit product subcategory')

@section('style')
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductSubcategoryController@index') }}">Product Subcategories</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit Product Category</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">


                <form id="sliderGroup" method="post" action="{{ action('Admin\ProductSubcategoryController@update',$product_subcategory->id) }}">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            @include('flash::message')
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Product Subcategory</h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Elements
                                            </legend>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ $product_subcategory->name }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Category Name</label>
                                                <select class="form-control" name="category_id">
                                                    @foreach($product_categories as $category)
                                                        <option value="{{ $category->id }}" @if($product_subcategory->category_id==$category->id){{ 'selected' }} @endif>{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    name : {
                        validators : {
                            notEmpty : {
                                message : 'Category name is required'
                            },
                        }
                    }
                }
            });

            // end profile form

        })
    </script>

    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
