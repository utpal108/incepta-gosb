@extends('admin.layout.app')

@section('page_title','Admin | Edit Footer')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\WidgetController@index') }}">Widgets</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit Widget</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" method="post" action="{{ action('Admin\WidgetController@update',$widget->name) }}">
                @csrf
                @method('put')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Footer</h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                First Section
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="contents[section_one][title]" value="{{ $widget->contents['section_one']['title'] ?? '' }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <textarea class="form-control" name="contents[section_one][details]" rows="5">{{ $widget->contents['section_one']['details'] ?? '' }}</textarea>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend>
                                                Second Section
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="contents[section_two][title]" value="{{ $widget->contents['section_two']['title'] ?? '' }}"/>
                                            </div>
                                            <div class="form-group row" id="socialIconGroup">
                                                <label class="col-md-12">Quick Links</label>

                                                @if(isset($widget->contents['section_two']['quick_links']) && !is_null($widget->contents['section_two']['quick_links']))
                                                    @php($sl=0)
                                                    @foreach($widget->contents['section_two']['quick_links'] as $quick_link)
                                                        <div class="col-md-12 eachSocialIcon mt-3">
                                                            <div class="input-group input-group-md">
                                                                <input type="text" class="form-control" name="contents[section_two][quick_links][{{ $sl }}][name]" placeholder="Name" value="{{ $quick_link['name'] }}" required>
                                                                <input type="text" class="form-control" name="contents[section_two][quick_links][{{ $sl }}][url]" placeholder="URL" value="{{ $quick_link['url'] }}" required>
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-default" type="button" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @php($sl++)
                                                    @endforeach

                                                @else
                                                    <div class="col-md-12 eachSocialIcon mt-3">
                                                        <div class="input-group input-group-md">
                                                            <input type="text" class="form-control" name="contents[section_two][quick_links][0][name]" placeholder="Name" required>
                                                            <input type="text" class="form-control" name="contents[section_two][quick_links][0][url]" placeholder="URL" required>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-default" type="button" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary pull-right" type="button" id="addSocialIcon">Add <i class="fa fa-plus-circle"></i></button>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend>
                                                Third Section
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="contents[section_three][title]" value="{{ $widget->contents['section_three']['title'] }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <textarea class="form-control" name="contents[section_three][details]" rows="5">{{ $widget->contents['section_three']['details'] }}</textarea>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // Add Section
            var socialElements= $('.eachSocialIcon').length;
            $('#addSocialIcon').click(function () {
                var data ='<div class="col-md-12 eachSocialIcon mt-3">\n' +
                    '                                                        <div class="input-group input-group-md">\n' +
                    '                                                            <input type="text" class="form-control" name="contents[section_two][quick_links]['+socialElements+'][name]" placeholder="Name" required>\n' +
                    '                                                            <input type="text" class="form-control" name="contents[section_two][quick_links]['+socialElements+'][url]" placeholder="URL" required>\n' +
                    '                                                            <div class="input-group-append">\n' +
                    '                                                                <button class="btn btn-default" type="button" onclick="if(confirm(\'Are You Sure ?\')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>\n' +
                    '                                                            </div>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>';
                $('#socialIconGroup').append(data);

                socialElements++;
            });


            // For Editor

            var editor_config = {
                path_absolute : "/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    group_name : {
                        validators : {
                            notEmpty : {
                                message : 'Group name is required'
                            },
                        }
                    }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
