
<div class="widget-body">
    <fieldset>
        <legend>
            Form Elements
        </legend>
        <div class="form-group">
            <label>Slider Title</label>
            <input type="text" class="form-control" name="contents[slider_title]" value="{{ $page->contents['slider_title'] ?? '' }}" required/>
        </div>
{{--        <div class="form-group">--}}
{{--            <label>Slider Subtitlet</label>--}}
{{--            <textarea class="form-control editor" name="contents[slider_subtitle]" rows="8">{!! $page->contents['slider_subtitle'] ?? '' !!}</textarea>--}}
{{--        </div>--}}
        <div class="form-group">
            <label>Slider Image</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['slider_image'])){{ '/storage/' .$page->contents['slider_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="slider image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="slider_image" @if(!isset($page->contents['slider_image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Page content</label>
            <textarea class="form-control editor" name="contents[page_content]" rows="8">{!! $page->contents['page_content'] ?? '' !!}</textarea>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


