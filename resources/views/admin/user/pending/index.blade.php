@extends('admin.layout.app')

@section('page_title','Admin | Pending Users')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\PendingUserController@index') }}">Pending Users</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Pending Users</span></h1>
            </div>
        </div>
        <div>
            <div>
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-12">
                        @include('flash::message')
                        <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <div class="widget-header">
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>Pending Users </h2>
                                    </div>
                                    <div class="widget-toolbar">
                                        <!-- add: non-hidden - to disable auto hide -->
                                    </div>
                                </header>
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body p-0">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone" class="text-center">ID</th>
                                                <th data-class="expand" class="text-center"> Name</th>
                                                <th data-class="expand" class="text-center">Type</th>
                                                <th data-class="expand" class="text-center"> Specialist</th>
                                                <th data-hide="phone, tablet" class="text-center"> Email</th>
                                                <th data-hide="phone, tablet" class="text-center"> Phone No</th>
                                                <th class="text-center"><i class="fa fa-fw fa-calendar text-blue hidden-md hidden-sm hidden-xs"></i> Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $index=>$user)
                                                <tr class="text-center">
                                                    <td>{{ $index+1 }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->user_type }}</td>
                                                    <td>{{ $user->specialist }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->phone_no }}</td>
                                                    <td>
                                                        <a class="btn btn-success btn-xs" href="{{ action('Admin\PendingUserController@approve_user',$user->id) }}">Approve</a>
                                                        <form action="{{ action('Admin\PendingUserController@destroy',$user->id) }}" method="post" style="display: inline">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger btn-xs" name="remove_slider_group" type="submit" onclick="deleteSliderGroup()">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                    <!-- Bootstrap Modal -->
                                    <div class="modal fade" id="confirm">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h2 class="modal-title">Delete pending user</h2>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    <h4>Are you sure ?</h4>
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn btn-danger" id="delete">Yes</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>
                        <!-- WIDGET END -->

                    </div>

                    <!-- end row -->

                    <!-- end row -->

                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteSliderGroup() {
            $('#myModal').modal("show");
        }

        $('button[name="remove_slider_group"]').on('click', function(e) {
            var $form = $(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '#delete', function(e) {
                    $form.trigger('submit');
                });
        });
    </script>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
