<!DOCTYPE html>

<html lang="en" class="smart-style-0">
<head>
    <title>Gosb | Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">
    <link rel="shortcut icon" href="/ic_admin/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/ic_admin/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/vendors.bundle.css">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/app.bundle.css">
    <link rel="stylesheet" type="text/css" href="/ic_admin/css/login.css">

</head>
<body class=" publicHeader-active animated fadeInDown smart-style-0">

<!-- BEGIN .sa-wrapper -->
<div class="sa-wrapper">
    <header id="header" class="publicheader">
        <div class="logo-group">
            <span class="sa-logo" style="margin-top: 10px"> <img src="/ic_admin/img/common/logo.png" alt="Logo"> </span>
        </div>

        {{--<span class="extr-page-header-space">--}}
            {{--<span class="hidden-mobile hiddex-xs">Need an account?</span>--}}
            {{--<a href="register.html" class="btn sa-btn-danger">Create account</a>--}}
        {{--</span>--}}
    </header>
    <div class="sa-page-body">
        <!-- BEGIN .sa-content-wrapper -->
        <div class="sa-content-wrapper">

            <div class="sa-content">

                <div class="main" role="main">

                    <!-- MAIN CONTENT -->
                    <div id="content" class="container padding-top-10">

                        <div class="row">
                            <div class="col-sm-12 col-lg-4 mx-auto">
                                @include('flash::message')
                                <div class="well no-padding">
                                    <form action="{{ action('UserController@authenticate') }}" method="post" id="login-form" class="smart-form client-form">
                                        @csrf
                                        <header>
                                            Sign In
                                        </header>

                                        <fieldset>

                                            <section>
                                                <label class="label">E-mail</label>
                                                <label class="input mb-3"> <i class="icon-append fa fa-user"></i>
                                                    <input type="email" name="email" required>
                                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                                            </section>

                                            <section>
                                                <label class="label">Password</label>
                                                <label class="input mb-1"> <i class="icon-append fa fa-lock"></i>
                                                    <input type="password" name="password" required>
                                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                                                <div class="note mb-3">
                                                    <a href="#">Forgot Password</a>
                                                </div>
                                            </section>

                                            <section>
                                                <label for="gra-0" class="vcheck mb-3">
                                                    <input type="checkbox" name="remember_token" value="true" id="gra-0" checked="checked">
                                                    <span></span> Stay signed in
                                                </label>
                                            </section>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn sa-btn-primary">
                                                Sign in
                                            </button>
                                        </footer>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END .sa-content-wrapper -->
    </div>

</div>
<!-- END .sa-wrapper -->

<script src="/ic_admin/js/vendors.bundle.js"></script>
<script src="/ic_admin/js/app.bundle.js"></script>

<script>
    $(function () {
        $('#menu1').metisMenu();
    });

//    For flash message
    $('div.alert').delay(3000).fadeOut(350);
</script>


</body>
</html>
