<ol class="dd-list">
    @foreach($items as $item)
        <li class="dd-item" data-id="{{ $item->id }}">
            <div class="pull-right" style="padding: 8px 0px;">
                <div class="btn btn-xs btn-danger pull-right button-delete" style="margin-left: 5px ; margin-right: 5px" data-id="{{ $item->id }}">
                    <i class="fa fa-trash"></i> Delete
                </div>
                <div class="btn btn-xs btn-primary pull-right button-edit" style="margin-right: 5px" data-id="{{ $item->id }}" data-title="{{ $item->title }}" data-target="{{ $item->target }}" data-url="{{ $item->url }}">
                    <i class="fa fa-edit"></i> Edit
                </div>
            </div>

            <div class="dd-handle">
                {{ $item->title }}
            </div>
            @if(!$item->children->isEmpty())
                @include('admin.menu.menu-items', ['items' => $item->children])
            @endif
        </li>
    @endforeach
</ol>
