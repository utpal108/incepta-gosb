@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')
    <section id="news-breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $page->contents['slider_title'] ?? ''}}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>


    <section id="news"><!-- news start -->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-main"><!-- Blog Details -->
                        <img src="/images/blog01.jpg" class="img-fluid">
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ulabore et dolore magna aliqua. </h5>
                        <p style="color: rgb( 0, 105, 145 )">Objectives of Gynaecological Oncology Society of Bangladesh (GOSB)</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiumod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiumod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiumod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiumod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <ul class="social-details">
                            <li>
                                <a href="#">Facebook 200</a>
                            </li>
                            <li>
                                <a href="#">Twitter 166</a>
                            </li>
                            <li>
                                <a href="#">Linkedin 200</a>
                            </li>
                            <li>
                                <a href="#">Pintrest 166</a>
                            </li>
                            <li>
                                <a href="#">Instagram 166</a>
                            </li>
                        </ul>
                        <div class="profile-detail"><!-- Profile Area start -->
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/images/profile.png" class="img-fluid" />
                                </div>
                                <div class="col-md-9">
                                    <h5>DR. Ronald Jackson</h5><p>Orthopeadic Consultant</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,  eiusmod temporiun incididunt ut labore et dolore magna aliqua. </p>
                                    <a href="#">Email Me</a>
                                </div>
                            </div>
                        </div><!-- Profile area End -->
                        <div class="about-btn" style="float: right">
                            <button class="btn btn-default"><a href="#">NEXT POST <i class="fas fa-arrow-right"></i></a></button>
                        </div>
                        <div class="comment"><!-- Comment area start -->
                            <h4>COMMENT (11)</h4>
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="/images/profile.png" class="img-fluid" />
                                </div>
                                <div class="col-md-10">
                                    <span>Shailly Amanda / 27 Dec 2018</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiumod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <a href="#">Reply</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="/images/profile.png" class="img-fluid" />
                                </div>
                                <div class="col-md-10">
                                    <span>Shailly Amanda / 27 Dec 2018</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiumod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <a href="#">Reply</a>
                                </div>
                            </div>
                            <p style="color: rgb( 0, 105, 145 )"><b>View More Comments</b></p>
                        </div><!-- Comment Area end -->
                        <div class="message-form"><!-- Message form area started -->
                            <h4>Leave A Comment Here</h4>

                            <div class="formBox">
                                <form>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="inputBox ">
                                                <input type="text" name="" class="input" placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="inputBox">
                                                <input type="text" name="" class="input" placeholder="Phone No">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="inputBox">
                                                <input type="text" name="" class="input" placeholder="Enter Your Email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="inputBox">
                                                <textarea rows="4" cols="50" placeholder="Enter Your MESSAGE"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="uh-button">
                                                <button class="btn btn-default">Sending Message</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>



                        </div><!-- Message form area endded -->


                    </div><!-- Blog Detail -->
                    <div class="pagination">
                        <img src="/images/pagination.png">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="side-bar"><!-- Side bar  -->
                        <div class="up-search"><!-- Srearch  -->
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search Here You Want To Know">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div> <!-- Search  -->
                        <div class="cat"><!-- Categories start -->
                            <h4>CATEGORIES</h4>
                            <ul>
                                <li><a href="#">&nbsp;Health</a></li>
                                <li><a href="#">&nbsp;Medical & Medicine</a></li>
                                <li><a href="#">&nbsp;Child Health</a></li>
                                <li><a href="#">&nbsp;Cancer</a></li>
                                <li><a href="#">&nbsp;Tumar</a></li>
                                <li><a href="#">&nbsp;Gynaecology</a></li>
                                <li><a href="#">&nbsp;Orthopeadic</a></li>
                                <li><a href="#">&nbsp;Kindney</a></li>
                            </ul>
                        </div><!-- Cotegories end -->
                        <div class="cat"><!-- Categories start 2-->
                            <h4>ARCHIVE</h4>
                            <ul>
                                <li><a href="#">&nbsp;2019</a></li>
                                <li><a href="#">&nbsp;2018</a></li>
                                <li><a href="#">&nbsp;2017</a></li>
                                <li><a href="#">&nbsp;2016</a></li>
                                <li><a href="#">&nbsp;December</a></li>
                                <li><a href="#">&nbsp;November</a></li>
                                <li><a href="#">&nbsp;October</a></li>
                                <li><a href="#">&nbsp;September</a></li>
                                <li><a href="#">&nbsp;August</a></li>
                            </ul>
                        </div><!-- Cotegories end 2-->
                        <div class="recent"><!-- Recent  1-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                        <div class="recent"><!-- Recent  2-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                        <div class="recent"><!-- Recent  3-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                    </div><!-- Side bar End -->
                </div>
            </div>
        </div>
    </section><!-- News End -->

@endsection
