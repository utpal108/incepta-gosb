@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')
    <section id="conference-breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $page->contents['slider_title'] ?? ''}}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>

    <section id="conference-main">
        <div class="container">
{{--            <div class="up-search conference-search">--}}
{{--                <div class="input-group">--}}
{{--                    <input type="text" class="form-control" placeholder="What Do You Want To Know">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <button class="btn btn-primary" type="button">--}}
{{--                            <i class="fa fa-search"></i>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row tb">--}}
{{--                <div class="col-md-4 pl">--}}
{{--                    <form>--}}
{{--                        <div class="form-group">--}}
{{--                            <select class="form-control" id="sel1">--}}
{{--                                <option>Upcomming Conference</option>--}}
{{--                                <option>2</option>--}}
{{--                                <option>3</option>--}}
{{--                                <option>4</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="col-md-4">--}}
{{--                    <form>--}}
{{--                        <div class="form-group">--}}
{{--                            <select class="form-control" id="sel1">--}}
{{--                                <option>Conference Type</option>--}}
{{--                                <option>2</option>--}}
{{--                                <option>3</option>--}}
{{--                                <option>4</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="col-md-4 pr">--}}
{{--                    <form>--}}
{{--                        <div class="form-group">--}}
{{--                            <select class="form-control" id="sel1">--}}
{{--                                <option>Conference Time</option>--}}
{{--                                <option>2</option>--}}
{{--                                <option>3</option>--}}
{{--                                <option>4</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}



            <div class="row"><!-- Table-Data -Show -->
                <table id="customers" class="table gfg">
                    <tr class="thead">
                        <th class="text-center" width="10%">SL#</th>
                        <th width="50%">TITLE AND INFORMATION</th>
                        <th width="20%">THEME</th>
                        <th width="20%">Country</th>
                    </tr>
                    @foreach($conferences as $index=>$conference)
                        <tr class="first-r" style="height:100px">
                            <td class="mid">{{ $index+1 }}</td>
                            <td> <p style="color: #1c1652;"><a href="{{ action('ConferenceController@details',$conference->id) }}" style="color: #1c1652">{{ strtoupper($conference->title ) }}</a></p>
                                <span class="ll">DAY: {{ $conference->conference_day ?? '' }} <br> TIME SESSION: {{ $conference->time_session ?? '' }}</span>
                            </td>
                            <td class="mid">{{ strtoupper($conference->theme) }}</td>
                            <td class="mid">{{ strtoupper($conference->country) }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
            <div class="pagination">
                {{ $conferences->links('pagination.specialist') }}
            </div>
        </div>
    </section><!-- Chirmen message End -->

@endsection
