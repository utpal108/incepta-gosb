@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')

    <section id="membership-breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $page->contents['slider_title'] ?? ''}}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>

    <section id="membership"><!-- Chairmen Message Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="membership-pera">
                        <h3><b>{{ $page->contents['page_title'] ?? '' }}</b></h3>
                        <img src="/images/dash.png"><br>
                        {!! $page->contents['page_details'] ?? '' !!}
                        <br>
                        <h5>Categories of membership of the society: </h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <!-- Accodion Start -->
            <div class="accordion" id="accordionExample">
                @foreach($memberships as $index=>$membership)
                    <div class="card">
                        <div class="card-header" id="heading{{ $index+1 }}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $index+1 }}" aria-expanded="true" aria-controls="collapse{{ $index+1 }}">
                                    {{ $index+1 .' . ' .$membership->title }}
                                </button>
                            </h5>
                        </div>

                        <div id="collapse{{ $index+1 }}" class="collapse {{ ($index==0) ? 'show' : '' }} " aria-labelledby="heading{{ $index+1 }}" data-parent="#accordionExample">
                            <div class="card-body">
                                {!! $membership->details !!}
                                @if(!is_null($membership->document))
                                    <a class="btn btn-default form-d" download="membership" href="{{ '/storage/' .$membership->document }}">Download Membership Form</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Accordion End -->
        </div>
    </section><!-- Chirmen message End -->

@endsection
