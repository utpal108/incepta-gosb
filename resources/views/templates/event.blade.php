@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')
    <section id="event-breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $page->contents['slider_title'] ?? ''}}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>


    <section id="news"><!-- news start -->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-main"><!-- Blog 1 -->
                        <img src="/images/Event01.jpg" class="img-fluid">
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ulabore et dolore magna aliqua. </h5>
                        <p style="color: rgb( 0, 105, 145 )">Healty Heart / Dhaka, Bangladesh / 20 April, 2019</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <div class="read-more">
                            <a hred="#"><b>READ MORE </b><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="blog-main"><!-- Blog 2 -->
                        <img src="/images/Event02.jpg" class="img-fluid">
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ulabore et dolore magna aliqua. </h5>
                        <p style="color: rgb( 0, 105, 145 )">Healty Heart / Dhaka, Bangladesh / 20 April, 2019</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <div class="read-more">
                            <a hred="#"><b>READ MORE </b><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="blog-main"><!-- Blog 3 -->
                        <img src="/images/Event03.jpg" class="img-fluid">
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ulabore et dolore magna aliqua. </h5>
                        <p style="color: rgb( 0, 105, 145 )">Healty Heart / Dhaka, Bangladesh / 20 April, 2019</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <div class="read-more">
                            <a hred="#"><b>READ MORE </b><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="blog-main"><!-- Blog 4 -->
                        <img src="/images/Event04.jpg" class="img-fluid">
                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ulabore et dolore magna aliqua. </h5>
                        <p style="color: rgb( 0, 105, 145 )">Healty Heart / Dhaka, Bangladesh / 20 April, 2019</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ulabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecnas accumsan lacus vel facilisis. </p>
                        <div class="read-more">
                            <a hred="#"><b>READ MORE </b><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="pagination">
                        <img src="/images/pagination.png"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="side-bar"><!-- Side bar  -->
                        <div class="up-search"><!-- Srearch  -->
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search Here You Want To Know">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div> <!-- Search  -->
                        <div class="cat"><!-- Categories start -->
                            <h4>CATEGORIES</h4>
                            <ul>
                                <li><a href="#">&nbsp;Health</a></li>
                                <li><a href="#">&nbsp;Medical & Medicine</a></li>
                                <li><a href="#">&nbsp;Child Health</a></li>
                                <li><a href="#">&nbsp;Cancer</a></li>
                                <li><a href="#">&nbsp;Tumar</a></li>
                                <li><a href="#">&nbsp;Gynaecology</a></li>
                                <li><a href="#">&nbsp;Orthopeadic</a></li>
                                <li><a href="#">&nbsp;Kindney</a></li>
                            </ul>
                        </div><!-- Cotegories end -->
                        <div class="cat"><!-- Categories start 2-->
                            <h4>ARCHIVE</h4>
                            <ul>
                                <li><a href="#">&nbsp;2019</a></li>
                                <li><a href="#">&nbsp;2018</a></li>
                                <li><a href="#">&nbsp;2017</a></li>
                                <li><a href="#">&nbsp;2016</a></li>
                                <li><a href="#">&nbsp;December</a></li>
                                <li><a href="#">&nbsp;November</a></li>
                                <li><a href="#">&nbsp;October</a></li>
                                <li><a href="#">&nbsp;September</a></li>
                                <li><a href="#">&nbsp;August</a></li>
                            </ul>
                        </div><!-- Cotegories end 2-->
                        <div class="recent"><!-- Recent  1-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                        <div class="recent"><!-- Recent  2-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                        <div class="recent"><!-- Recent  3-->
                            <h4>RECENT POST</h4>
                            <p>we build together future architect enginering building</p>
                            <span>By John Doe / MArch 28,2019</span>
                        </div><!-- Recent -->
                    </div><!-- Side bar End -->
                </div>
            </div>
        </div>
    </section><!-- News End -->

@endsection
