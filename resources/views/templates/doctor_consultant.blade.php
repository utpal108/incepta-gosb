@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')

    <section id="consultant-breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $page->contents['slider_title'] }}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>

    <section id="consultant"><!-- Chairmen Message Start -->
        <div id="related-doctors"><!--  Related Doctors Area Start -->
            <div class="container">
                <div class="doc-heading">
                    <h3><b>{{ $page->contents['page_title'] ?? '' }}</b></h3>
                    <img src="/images/dash.png" class="img-fluid"/>
                </div>
                <div class="row fiw">
                    @foreach($doctors as $doctor)
                        @if(!is_null($doctor->profile))
                            <div class="col-md-3">
                                <a href="{{ action('DoctorController@details',$doctor->id) }}"><img src="/storage/{{ $doctor->profile_image }}" class="img-fluid"/></a>
                                <div class="box-detail">
                                    <h5><a class="dark" href="{{ action('DoctorController@details',$doctor->id) }}">{{ $doctor->name }}</a></h5>
                                    <span><a class="dark" href="{{ action('DoctorController@details',$doctor->id) }}">{{ $doctor->specialist }}</a></span>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div><!-- Related Doctors Area End -->
        <div class="pagination">
            {{ $doctors->links('pagination.specialist') }}
        </div>
    </section>

@endsection
