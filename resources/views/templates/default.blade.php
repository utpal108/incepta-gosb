@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')
    <section id="breadcrum" style="background-image: url({{ '/storage/'.$page->contents['slider_image'] ?? '' }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{$page->contents['slider_title']  }}</h2>
                    <strong><a href="/">HOME</a> // {{ strtoupper($page->page_title) }}</strong>
                </div>
            </div>
        </div>
    </section>

    {!! $page->contents['page_content'] ?? '' !!}
@endsection
