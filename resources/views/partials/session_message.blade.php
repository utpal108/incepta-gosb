
<div class="modal" id="success_message">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="document.getElementById('success_message').style.display='none'" >&times;</button>
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>{{ session('success_message') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('success_message').style.display='none'" >Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="warning_message">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="document.getElementById('warning_message').style.display='none'" >&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <p>{{ session('warning_message') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('warning_message').style.display='none'" >Close</button>
            </div>
        </div>
    </div>
</div>



