
<div id="banner" class="container-fluid">
    <div class="banner-slider">
        @foreach($sliders as $slider)
            <div class="slide-1" style="background-image: url({{ '/storage/' .$slider->slider_image }})">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7   col-sm-7">
                            <h1>{{ $slider->title ?? '' }}</h1>
                            <h2>{{ $slider->subtitle ?? '' }}</h2>
                            {!! $slider->details ?? '' !!}
{{--                            <div class="uh-button">--}}
{{--                                <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-default">JOIN NOW</button>--}}
{{--                            </div>--}}
                        </div>
                        <div class="col-md-5 col-sm-5"></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div><!-- Banner End -->
