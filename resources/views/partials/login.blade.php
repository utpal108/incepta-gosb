<div id="id01" class="modal">

    <form class="modal-content animate" action="{{ action('UserController@user_authenticate') }}" method="post">
        @csrf
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="{{ '/storage/'.setting('logo') ?? '' }}" alt="Avatar" style="width: 100px" height="100px">
            <h3>LOG IN AS</h3>
        </div>

        <div class="container">
            <div class="text-center">
                <label class="radio-inline after-1">
                    <input type="radio" name="user_type" value="doctor" checked>DOCTOR
                </label>
                <label class="radio-inline after-2">
                    <input type="radio" name="user_type" value="consultant">CONSULTANT
                </label>
                <label class="radio-inline after-3">
                    <input type="radio" name="user_type" value="user">USER
                </label>
            </div>

            <input type="text" placeholder="Email Address" name="email" required>
            <input type="password" placeholder="Password" name="password" required>

            <button type="submit" class="btn btn-default login-btn" >Login Into My Accounts</button>
        </div>

        <div class="container pd">
            <p style="text-align: center;">Dont Have a Account Yet? <a href="#" class="registar">Register Now</a></p>
        </div>
    </form>
</div>
