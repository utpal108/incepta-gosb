<ul class="dropdown-menu">
    @foreach($items as $item)
        <li class="nav-item {{ request()->is($item->url) ? 'active' : '' }}">
            <a class="nav-link" href="/{{ $item->url }}">{{ strtoupper($item->title) }} @if(!$item->children->isEmpty())<span class="sr-only">(current)</span>@endif</a>
            @if(!$item->children->isEmpty())
                @include('partials.menu-items', ['items' => $item->children])
            @endif
        </li>
    @endforeach
</ul>
