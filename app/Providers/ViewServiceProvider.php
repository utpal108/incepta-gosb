<?php

namespace App\Providers;

use App\BusinessPartner;
use App\Conference;
use App\Membership;
use App\Menu;
use App\Slider;
use App\User;
use App\Widget;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //        For Sliders
        view()->composer('partials.slider', function($view) {
            $sliders=Slider::all();
            $view->with(['sliders'=>$sliders]);
        });

        //        For Footer
        view()->composer('partials.footer', function($view) {
            $footer=Widget::where('name','footer')->first();
            $view->with(['footer'=>$footer]);
        });

        //	    For Menu
        view()->composer('partials.menu', function($view) {
            $items=Menu::display('admin');
            $view->with(['items'=>$items]);
        });

        //	    For Doctors
        view()->composer('templates.doctor_consultant', function($view) {
            $doctors=User::whereIn('user_type',['doctor','consultant'])->paginate(5);
            $view->with(['doctors'=>$doctors]);
        });

        //	    For Memberships
        view()->composer('templates.membership', function($view) {
            $memberships=Membership::all();
            $view->with(['memberships'=>$memberships]);
        });

        //	    For Conferences
        view()->composer('templates.conference', function($view) {
            $conferences=Conference::paginate(5);
            $view->with(['conferences'=>$conferences]);
        });
    }
}
