<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    protected $fillable=['title','theme','country','conference_type_id','conference_day','time_session','details','extra_data'];
}
