<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function login(){
        return view('admin.user.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->action('UserController@login');
    }

    public function authenticate(Request $request){
        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=>'admin'])){
            return redirect()->action('Admin\DashboardController@index');
        }
        else{
            flash('Invalid username or password')->error();
            return redirect()->back();
        }
    }

    public function user_authenticate(Request $request){
        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=>$request->user_type, 'status'=>'approved'])){
            return redirect()->back();
        }
        else{
            session()->flash('warning_message','Invalid username or password');
            return redirect()->back();
        }
    }

    public function user_logout(){
        Auth::logout();
        return redirect('/');
    }

    public function store(Request $request){
        $validation=Validator::make($request->all(), [
            'email' => 'required|unique:users',
        ]);

        if ($validation->fails()){
            session()->flash('warning_message', 'User are already exist');
        }
        $allData=$request->all();
        $allData['status']='pending';
        $allData['password']=bcrypt($request->password);
        User::create($allData);
        session()->flash('success_message', 'Register successfully. Please wait for admin approval');
        return redirect()->back();
    }

    public function edit_profile(){
        $user=User::with('profile')->where('id',Auth::id())->first();
        return view('user.edit',compact('user'));
    }

    public function update_profile(Request $request){
        $user=User::find(Auth::id());
        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(445, 520)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }
        $user->name=$request->name;
        $user->designation=$request->designation;
        $user->specialist=$request->specialist;
        $user->phone_no=$request->phone_no;
        $user->save();

        Profile::updateOrCreate(['user_id'=>$user->id],
            [
                'about'=>$request->about,
                'education'=>$request->education,
                'experience'=>$request->experience,
                'awards'=>$request->awards,
                'address'=>$request->address,
                'website'=>$request->website,
                'social_links'=>$request->social_links,
            ]
        );
        flash('Profile updated successfully');
        return view('user.edit',compact('user'));
    }

}
