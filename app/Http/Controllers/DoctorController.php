<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function details($id){
        $doctor=User::find($id);
        return view('templates.doctor_details',compact('doctor'));
    }
}
