<?php

namespace App\Http\Controllers;

use App\Conference;
use Illuminate\Http\Request;

class ConferenceController extends Controller
{
    public function details($id){
        $conference=Conference::find($id);
        return view('templates.conference_details',compact('conference'));
    }
}
