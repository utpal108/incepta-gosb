<?php

namespace App\Http\Controllers\Admin;

use App\BlogPost;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog_posts=BlogPost::all();
        return view('admin.blog.index',compact('blog_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.blog.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        $allData['created_by']=Auth::id();
        if ($request->hasFile('feature_image')){
            $path=$request->file('feature_image')->store('images/blog');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $allData['feature_image']=$path;
        }

        BlogPost::create($allData);
        flash('Blog post created successfully');
        return redirect()->action('Admin\BlogPostController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::all();
        $blog_post=BlogPost::find($id);
        return view('admin.blog.edit',compact('blog_post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog_post=BlogPost::find($id);
        if ($request->hasFile('feature_image')){
            Storage::delete($blog_post->feature_image);
            $path=$request->file('feature_image')->store('images/blog');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $blog_post->feature_image=$path;
        }

        $blog_post->title=$request->title;
        $blog_post->category_id=$request->category_id;
        $blog_post->details=$request->details;
        $blog_post->updated_by=Auth::id();
        $blog_post->save();
        flash('Blog post updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog_post=BlogPost::find($id);
        Storage::delete($blog_post->feature_image);
        BlogPost::destroy($id);
        flash('Blog post deleted successfully');
        return redirect()->action('Admin\BlogPostController@index');
    }
}
