<?php

namespace App\Http\Controllers\Admin;

use App\Membership;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memberships=Membership::all();
        return view('admin.membership.index',compact('memberships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.membership.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        if ($request->hasFile('document')){
            $allData['document']=$request->file('document')->store('files');
        }
        Membership::create($allData);
        flash('Membership created successfully');
        return redirect()->action('Admin\MembershipController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $membership=Membership::find($id);
        return view('admin.membership.edit',compact('membership'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $membership=Membership::find($id);
        $membership->title=$request->title;
        $membership->details=$request->details;
        if ($request->hasFile('document')){
            Storage::delete($membership->document);
            $membership->document=$request->file('document')->store('files');
        }
        $membership->save();
        flash('Membership updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $membership=Membership::find($id);
        Storage::delete($membership->document);
        Membership::destroy($id);
        flash('Membership deleted successfully');
        return redirect()->action('Admin\MembershipController@index');
    }

    public function remove_image($id){
        $membership=Membership::find($id);
        Storage::delete($membership->document);
        $membership->document=null;
        $membership->save();
        flash('Document removed successfully');
        return redirect()->back();
    }
}
