<?php

namespace App\Http\Controllers\Admin;

use App\ConferenceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConferenceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference_types=ConferenceType::all();
        return view('admin.conference.type.index',compact('conference_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.conference.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ConferenceType::create($request->all());
        flash('Conference type created successfully');
        return redirect()->action('Admin\ConferenceTypeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conference_type=ConferenceType::find($id);
        return view('admin.conference.type.edit',compact('conference_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $conference_type=ConferenceType::find($id);
        $conference_type->name=$request->name;
        $conference_type->save();
        flash('Conference Type Updated Successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ConferenceType::destroy($id);
        flash('Conference type deleted successfully');
        return redirect()->action('Admin\ConferenceTypeController@index');
    }
}
