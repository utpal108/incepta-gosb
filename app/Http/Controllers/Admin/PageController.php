<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\PageTemplate;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages=Page::all();
        return view('admin.pages.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_templates=PageTemplate::all();
        return view('admin.pages.create', compact('page_templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'page_title' => 'required|unique:pages',
        ]);
        $allData=$request->all();
        $allData['slug']=str_replace(' ', '-', $request->slug);
        $page_template=PageTemplate::find($request->template_id);
        if ( in_array($page_template->template_name, ['default', 'event', 'blog', 'blog_details','doctor_consultant','doctor_details','membership','conference'])){
            if ($request->hasFile('slider_image')){

                $path=$request->file('slider_image')->store('images');
                $image = Image::make(Storage::get($path))->fit(1920, 300)->encode();
                Storage::put($path, $image);
                $allData['contents']['slider_image']=$path;
            }
        }


        Page::create($allData);
        flash('Page Created Successfully');
        return redirect()->action('Admin\PageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_templates=PageTemplate::all();
        $page=Page::with('template')->find($id);
        return view('admin.pages.edit', compact('page','page_templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(), [
            'page_title' => [
                'required',
                Rule::unique('pages')->ignore($id),
            ],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $allContents=$request->contents;
        $page=Page::with('template')->find($id);

        if (in_array($page->template['template_name'], ['default', 'event', 'blog', 'blog_details','doctor_consultant','doctor_details','membership','conference'])){
            if ($request->hasFile('slider_image')){
                if (isset($page->contents['slider_image'])){
                    Storage::delete($page->contents['slider_image']);
                }
                $path=$request->file('slider_image')->store('images');
                $image = Image::make(Storage::get($path))->fit(1920, 300)->encode();
                Storage::put($path, $image);
                $allContents['slider_image']=$path;
            }
            else{
                $allContents['slider_image']=$page->contents['slider_image'];
            }
        }


        $page->page_title=$request->page_title;
        $page->slug=str_replace(' ', '-', $request->slug);
        $page->template_id=$request->template_id;
        $page->contents=$allContents;
        $page->save();
        flash('Page updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        flash('Page deleted successfully');
        return redirect()->action('Admin\PageController@index');
    }

    public  function load_template(Request $request){
        $selected_template=PageTemplate::find($request->template_id);
        $page_action= $request->type;
        $page='';
        if (isset($request->page_id)){
            $page=Page::find($request->page_id);
        }

        if ($selected_template){
            return view('admin.template.'.$selected_template->template_name,compact('page_action','page'));
        }
        else{
            return false;
        }

    }
}
