<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        return view('admin.event.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.event.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        $allData['created_by']=Auth::id();
        if ($request->hasFile('feature_image')){
            $path=$request->file('feature_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $allData['feature_image']=$path;
        }
        Event::create($allData);
        flash('New event created successfully');
        return redirect()->action('Admin\EventController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event=Event::find($id);
        $categories=Category::all();
        return view('admin.event.edit',compact('event','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event=Event::find($id);
        $event->title=$request->title;
        $event->category_id=$request->category_id;
        $event->details=$request->details;
        $event->updated_by=Auth::id();
        if ($request->hasFile('feature_image')){
            Storage::delete($event->feature_image);
            $path=$request->file('feature_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $event->feature_image=$path;
        }
        $event->save();
        flash('Event updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        Storage::delete($event->feature_image);
        Event::destroy($id);
        return redirect()->action('Admin\EventController@index');
    }
}
