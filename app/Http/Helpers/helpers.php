<?php

use App\SiteSetting;

if (! function_exists('setting')) {
        function setting($key = null)
        {
            $settings=SiteSetting::all()->toArray();
            if (is_array($settings)){
                $allData=array_column($settings,'value','key');
                if (isset($allData[$key])){
                    return $allData[$key];
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }

        }
    }
